package main

import (
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"os"
)

func main() {
	logger := slog.New(slog.NewTextHandler(os.Stdout, nil))

	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		// if we can't use the default port, 8080, then try and get another random port
		listener, err = net.Listen("tcp", ":0")
	}
	// otherwise, error
	if err != nil {
		fatal(logger, err)
	}

	port := listener.Addr().(*net.TCPAddr).Port

	dir, err := os.Getwd()
	if err != nil {
		fatal(logger, err)
	}

	logger.Info(fmt.Sprintf("Running `serve` on http://localhost:%d", port), "url", fmt.Sprintf("http://localhost:%d", port), "dir", dir)

	http.Handle("/", http.FileServer(http.Dir(".")))

	err = http.Serve(listener, nil)
	if err != nil {
		fatal(logger, err)
	}
}

func fatal(logger *slog.Logger, err error) {
	logger.Error(err.Error(), "err", err)
	os.Exit(1)
}
